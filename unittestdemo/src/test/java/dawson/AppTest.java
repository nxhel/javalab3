package dawson;
//import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class AppTest 
{
    @Test
    public void testEcho()
    {
        assertEquals("method should return a the parameter",5,App.echo(5) );
    }

    @Test
    public void testOneMore()
    {
        assertEquals("method should return a number greater than the parameter",6,App.oneMore(5) );
    }
}
